# Docker image for DD1388

This repository holds the specification for the docker image `bohrnsen/dd1388` which is used for the course DD1388 at the Royal Institute of Technology in Stockholm.

## Installed packages

- man pages
- g++
- clang
- cxxtest
- valgrind
- gdb
- cmake
- make
