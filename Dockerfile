FROM ubuntu:latest

LABEL maintainer="Kai Böhrnsen <bohrnsen@kth.se>"

ENV DEBIAN_FRONTEND=noninteractive

ENV TZ=Europe/Stockholm
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
        clang \
        cmake \
        cxxtest \
        gdb \
    	g++ \
        lcov \
        make \
        man-db \
        manpages-dev \
	    manpages-posix-dev \
        valgrind && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /labs